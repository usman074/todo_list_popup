import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodoListComponent } from './todo-list/todo-list.component'
import { AddTodoComponent } from './add-todo/add-todo.component';
const routes: Routes = [
  { path: 'addTask', component: AddTodoComponent },
  { path: 'todoList', component: TodoListComponent },
  { path: 'updateTask/:id', component: AddTodoComponent },
  { path: '', redirectTo: '/todoList', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
