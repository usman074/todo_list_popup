import { Component, OnInit } from '@angular/core';
import { TaskListService } from '../task-list.service';
import { taskStructure } from '../taskStructure';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.css']
})
export class AddTodoComponent implements OnInit {

  constructor(private taskList : TaskListService, private location : Location, private route: ActivatedRoute, private router: Router) { }
  tasks : taskStructure[] = [];
  taskValue : string = null;
  btn_name : string;
  ngOnInit() {
    if(this.route.snapshot.url[0].path === "addTask")
    {
      this.btn_name = "Save";
    }
    else if(this.route.snapshot.url[0].path === "updateTask"){
      this.btn_name = "Update";
      this.getTaskValue();
    }
  }
  getTaskValue(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    if(id){
      this.taskValue = this.taskList.getTask(id)
    }
    
  }

  savetask(taskName : string) : void{
    if(this.btn_name === "Save")
    {
      this.taskList.addTask(taskName);
    }
    else if(this.btn_name === "Update"){
      const id = +this.route.snapshot.paramMap.get('id');
      this.taskList.updateTaskName(id,taskName);
      this.btn_name = "Save"
    }
    this.router.navigateByUrl('/todoList');
  }
  goBack(): void {
    this.location.back();
  }

}
