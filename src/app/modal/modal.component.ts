import { Component, OnInit, Input } from '@angular/core';
import { TaskListService } from '../task-list.service';
import { taskStructure } from '../taskStructure';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  @Input() modalTitle : string;
  @Input() modalButtonName : string;
  @Input() taskValue : string;
  @Input() selectedTask : taskStructure;
  constructor(private taskList : TaskListService) { 
  }

  ngOnInit() {
  }
  savetask(taskName : string) : void{
    if(this.modalButtonName === "Save")
    {
      this.taskList.addTask(taskName);
    }
    else if(this.modalButtonName === "Update"){
      this.taskList.updateTaskName(this.selectedTask.id,taskName);
      // this.modalButtonName = "Save"
    }
  }
}
