import { Component, OnInit } from '@angular/core';
import { TaskListService } from '../task-list.service';
import { taskStructure } from '../taskStructure';
@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.css']
})
export class TodoListComponent implements OnInit {

  constructor(private taskList : TaskListService) { }

  tasks : taskStructure[] = [];
  tasksCompleted : taskStructure[] =[];
  modalTitle:string;
  modalButtonName : string;
  selectedTask : taskStructure;
  taskValue: string;
  ngOnInit() {
    this.tasks = this.taskList.getTaskList();
  }

  delTask(id : number) : void{
    this.tasks = this.taskList.delTask(id);
  }
  onCompleted(task : taskStructure) : void{
    this.tasksCompleted.push(task);
    this.tasks = this.taskList.updateTask(task.id);
  }

  setModal(btn_click : string, task : taskStructure) : void {
    if(btn_click === 'addTask')
    {
      this.modalTitle = "Add Task";
      this.modalButtonName = "Save";
      this.taskValue = null;
    }
    else if(btn_click === 'editTask')
    {
      this.modalTitle = "Update Task";
      this.modalButtonName = "Update";
      this.selectedTask = task;
      this.taskValue = task.name;
    }
  }

  // savetask(taskName : string) : void{
  //   if(this.modalButtonName === "Save")
  //   {
  //     this.taskList.addTask(taskName);
  //   }
  //   else if(this.modalButtonName === "Update"){
  //     this.taskList.updateTaskName(this.selectedTask.id,taskName);
  //     this.modalButtonName = "Save"
  //   }
  // }
}
